%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include "y.tab.h"
%}

%option noyywrap
%%

";"  {return SEMICOLON;}
":"  {return COLON;}
","  {return COMMA;}
"{"  {return OPEN_CURLY_BRACES;}
"}"  {return CLOSE_CURLY_BRACES;}
"<"  {return LESS_THAN;}
">"  {return GREATER_THAN;}
"("  {return OPEN_PARENTHESES;}
")"  {return CLOSE_PARENTHESES;}
"["  {return OPEN_BRACKET;}
"]"  {return CLOSE_BRACKET;}
"+"  {return PLUS;}
"-"  {return MINUS;}
"*"  {return MULTIPLY;}
"/"  {return DIVIDE;}
"%"  {return MOD;}
"="  {return EQUAL;}
"#"  {return NUMERAL;}
"!="  {return NOT_EQUAL;}
">="  {return GREATER_OR_EQUAL;}
"<="  { return LESS_OR_EQUAL;}

"||"  {return OR;}
"&&"  {return AND;}

"return"  {return RETURN;}
"define"  {return DEFINE;}
"for"  {return FOR;}
"while" {return WHILE;}
"if"   {return IF;}
"else"  {return ELSE;}
"elif"  {return ELSE_IF;}

"main"  {return MAIN;}

"true"   {yylval = 1; return BOOLEAN;}
"false"  {yylval = 0; return BOOLEAN;}

-?[0-9]+						{if(yytext[0] == '-') {
                      yylval = -1;
                      yytext = yytext + 1;
                    } else {
                      yylval = 1;
                    }
                    yylval *= atoi(yytext);
								    return INTEGER;}

[a-zA-Z]([a-zA-Z0-9_])*		{yylval = malloc(yyleng + 1);
                          memcpy(yylval, yytext, yyleng);
                          yylval[yyleng] = '\0';
								          return NAME;}

\"[^\"]*\"					{yylval = malloc(yyleng + 1);
                    memcpy(yylval, yytext, yyleng);
                    yylval[yyleng] = '\0';
								    return STRING;}

\/\*[^\*\/]*\*\/    ;
\/\/[^\n]*  ;
[ \t]  ;

\n  {yylineno++;}

%%
